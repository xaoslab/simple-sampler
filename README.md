# Simple WebAudio API sampler

Built with WebAudio oscillator.

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## See more

[chaos lab.](https://xaoslab.tech)