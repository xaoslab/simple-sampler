'use strict';
var otype = 'sine';
var ofreq = $('#freq').val();
const ac = new(window.AudioContext || window.webkitAudioContext)();
var oscillator = ac.createOscillator();
var merger = ac.createChannelMerger(2);
oscillator.type = otype;
oscillator.frequency.setValueAtTime(440, ac.currentTime);

var gainL = ac.createGain();
var gainR = ac.createGain();

oscillator.connect(gainR);
oscillator.connect(gainL);

gainL.connect(merger, 0, 0);
gainR.connect(merger, 0, 1);

oscillator.start();
gainR.gain.value = $('#rval').val();
gainL.gain.value = $('#lval').val();

merger.connect(ac.destination);

function setGains() {
    gainR.gain.value = $('#rval').val();
    gainL.gain.value = $('#lval').val();
    oscillator.frequency.setValueAtTime($('#freq').val(), ac.currentTime);
}
